var express=require('express')
var con=require('./db')
var bcrypt = require('bcrypt');
var mysql = require('mysql');
var router=express.Router();

Web3 = require('web3');
const Tx = require('ethereumjs-tx');
var balance = 0 ;
var abi =
    [
        {
            "constant": true,
            "inputs": [],
            "name": "name",
            "outputs": [
                {
                    "name": "",
                    "type": "string"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "spender",
                    "type": "address"
                },
                {
                    "name": "tokens",
                    "type": "uint256"
                }
            ],
            "name": "approve",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "totalSupply",
            "outputs": [
                {
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "sender",
                    "type": "address"
                },
                {
                    "name": "player",
                    "type": "address"
                },
                {
                    "name": "gameCoins",
                    "type": "uint256"
                }
            ],
            "name": "win",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "from",
                    "type": "address"
                },
                {
                    "name": "to",
                    "type": "address"
                },
                {
                    "name": "tokens",
                    "type": "uint256"
                }
            ],
            "name": "transferFrom",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "decimals",
            "outputs": [
                {
                    "name": "",
                    "type": "uint8"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [
                {
                    "name": "game",
                    "type": "address"
                }
            ],
            "name": "getGameUnitPrice",
            "outputs": [
                {
                    "name": "gameUnit",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "game",
                    "type": "address"
                },
                {
                    "name": "unit",
                    "type": "uint256"
                }
            ],
            "name": "setGameUnitPrice",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "_totalSupply",
            "outputs": [
                {
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "sender",
                    "type": "address"
                },
                {
                    "name": "player",
                    "type": "address"
                },
                {
                    "name": "gameCoins",
                    "type": "uint256"
                }
            ],
            "name": "lose",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "sender",
                    "type": "address"
                },
                {
                    "name": "game",
                    "type": "address"
                },
                {
                    "name": "coins",
                    "type": "uint256"
                }
            ],
            "name": "topUp",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [
                {
                    "name": "tokenOwner",
                    "type": "address"
                }
            ],
            "name": "balanceOf",
            "outputs": [
                {
                    "name": "balance",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [],
            "name": "acceptOwnership",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "sender",
                    "type": "address"
                },
                {
                    "name": "player",
                    "type": "address"
                },
                {
                    "name": "gameCoins",
                    "type": "uint256"
                }
            ],
            "name": "cashOut",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "owner",
            "outputs": [
                {
                    "name": "",
                    "type": "address"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "symbol",
            "outputs": [
                {
                    "name": "",
                    "type": "string"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "to",
                    "type": "address"
                },
                {
                    "name": "tokens",
                    "type": "uint256"
                }
            ],
            "name": "transfer",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [
                {
                    "name": "tokenOwner",
                    "type": "address"
                },
                {
                    "name": "game",
                    "type": "address"
                }
            ],
            "name": "gameBalanceOf",
            "outputs": [
                {
                    "name": "balance",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "spender",
                    "type": "address"
                },
                {
                    "name": "tokens",
                    "type": "uint256"
                },
                {
                    "name": "data",
                    "type": "bytes"
                }
            ],
            "name": "approveAndCall",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "newOwner",
            "outputs": [
                {
                    "name": "",
                    "type": "address"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "tokenAddress",
                    "type": "address"
                },
                {
                    "name": "tokens",
                    "type": "uint256"
                }
            ],
            "name": "transferAnyERC20Token",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [
                {
                    "name": "tokenOwner",
                    "type": "address"
                },
                {
                    "name": "spender",
                    "type": "address"
                }
            ],
            "name": "allowance",
            "outputs": [
                {
                    "name": "remaining",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "_newOwner",
                    "type": "address"
                }
            ],
            "name": "transferOwnership",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "constructor"
        },
        {
            "payable": true,
            "stateMutability": "payable",
            "type": "fallback"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "name": "_from",
                    "type": "address"
                },
                {
                    "indexed": true,
                    "name": "_to",
                    "type": "address"
                }
            ],
            "name": "OwnershipTransferred",
            "type": "event"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "name": "from",
                    "type": "address"
                },
                {
                    "indexed": true,
                    "name": "to",
                    "type": "address"
                },
                {
                    "indexed": false,
                    "name": "tokens",
                    "type": "uint256"
                }
            ],
            "name": "Transfer",
            "type": "event"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "name": "tokenOwner",
                    "type": "address"
                },
                {
                    "indexed": true,
                    "name": "spender",
                    "type": "address"
                },
                {
                    "indexed": false,
                    "name": "tokens",
                    "type": "uint256"
                }
            ],
            "name": "Approval",
            "type": "event"
        }
    ];
var contract;


const multer = require('multer');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/')
    },
    filename: (req, file, cb) => {
        cb(null,file.originalname+ Date.now())
    }
});
var upload = multer({storage: storage});


const fileFilter = (req, file, cb) => {

    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'|| file.mimetype === 'application/pdf' ){

        cb(null, true);
    }
    else {
        cb(null, false);


    };
}

var expires = new Date();
var contractAddress="0x9378e4cf1b5991eb5f1483c885426a9d23d87534";
var connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "playcoin"
});

var nodemailer = require('nodemailer');


connection.connect(function (err) {

    if (err) throw err
    console.log('You are now connected...')

})

router.get('/',function (req,res) {

      res.send("okkkkk user ")
})





router.post('/loginUser',function (req,res) {


    var email = req.query.email;
    var password = req.query.password;
    var sql = 'SELECT * FROM user WHERE email = ?';
    con.myConn().query(sql, [email], function (err, result) {
        if (err) throw err;

        else{
            if (result[0] != null) {


                var hash = result[0].password;
                bcrypt.compare(password, hash, function(err, doesMatch){
                    if (doesMatch){  res.send(result[0])


                        /*   req.session.user_id = result[0].id;
                           req.session.user_username = result[0].username;
                           req.session.user_email = result[0].email;
                           req.session.user_account_type = result[0].account_type;
                           req.session.user_image = result[0].image;
                           req.session.user_address = result[0].address;
                           req.session.user_private_key = result[0].private_keys;
                           if (req.session.user_account_type == "game") {
                               req.session.user_game_description = result[0].game_description;
                               req.session.user_game_type = result[0].game_type;
                           }
       */

                    }
                    else {  res.send("mot d pass incorrect")}


                });
            }


            else{console.log("n exist pas");
                res.send("user n'exist pas")}
        }

    });




})




/*************** get All ****************************/
router.get('/allGames',function (req,res) {

    con.myConn().query("SELECT * FROM user where account_type='game'", function (err, result, fields) {
          if (err) throw err;
          console.log(result);
          res.send(JSON.stringify(result))
      });


})


/****************************************************/
/******************** get by id**********************/



router.get('/getUserNamesOfTransaction',function (req,res) {


    var id = req.query.sender_id;
    var sql = 'SELECT username FROM user a INNER JOIN transaction b ON a.id=b.receiver WHERE b.sender= ?';
    con.myConn().query(sql, [id], function (err, result) {
        if (err) throw err;
        console.log(result);
        res.send(result)
    });


})

router.get('/getTransactionByID',function (req,res) {


    var id = req.query.sender_id;
    var sql = 'SELECT * FROM transaction WHERE sender = ?';
    con.myConn().query(sql, [id], function (err, result) {
        if (err) throw err;
        console.log(result);
        res.send(result)
    });


})




router.get('/getTransactions',function (req,res) {

    var listTransaction =[];
    var id = req.query.sender_id;
    var sql = 'SELECT * FROM transaction WHERE sender = ?';
    con.myConn().query(sql, [id], function (err, result) {
        if (err){ throw err;}
        else{    console.log(result);
     var z=0;
        while(z<result.length)
        {

            var username ;
            var sql = "SELECT * FROM `user` WHERE id=?"
            con.myConn().query(sql, [43], function (error, username) {
                if(!error)
                    console.log("result",result[1].id , "compteuur ",z);
                    //listTransaction.push({id:result[i].id , sender:result[i].sender, receiver:username[i].username,ammount:result[i].ammount})
                else
                    console.log(error);

            });

            z=z+1;

        }


        }

        res.send(listTransaction)
    });


})


router.get('/getUserById',function (req,res) {


    var id = req.query.id;
    var sql = 'SELECT * FROM user WHERE id = ?';
    con.myConn().query(sql, [id], function (err, result) {
        if (err) throw err;
        console.log(result);

        res.send(JSON.stringify(result[0]))
    });




})


router.post('/getGamesById',function (req,res) {

var myGames = []
    var id = req.query.user_id;
    var sql = "SELECT * FROM `user` WHERE id IN (select game FROM user_games WHERE user =?)"
    con.myConn().query(sql, [id], function (err, result) {
        if (err) throw err;
        console.log(result);

        res.send(result)
    });




})




router.post('/getUsersByGame',function (req,res) {

    var myGames = []
    var id = req.query.game_id;
    var sql = "SELECT * FROM `user` WHERE id IN (select user FROM user_games WHERE game =?)"
    con.myConn().query(sql, [id], function (err, result) {
        if (err) throw err;
        console.log(result);

        res.send(result)
    });




})


router.get('/getAddressByEmail',function (req,res) {


    var email = req.query.email;
    var sql = "SELECT * FROM `user` WHERE email=?"
    con.myConn().query(sql, [email], function (err, result) {
        if (err) throw err;
        console.log(result[0]);

        res.send(JSON.stringify(result[0]))
    });




})



router.get('/getUsernameByID',function (req,res) {


    var id = req.query.id;
    var username ;
    var sql = "SELECT * FROM `user` WHERE id=?"
    con.myConn().query(sql, [id], function (err, result) {
        if(!err)
            username = result[0]
        else
            console.log(err);
        res.send(JSON.stringify(result[0]))
    });




})
/****************************************************************/
/******************* insert ************************************/
router.get('/addGameToMyGames',function (req,res) {


    var user_id = req.query.user_id;
    var game_id = req.query.game_id;
    var sql = "INSERT INTO `user_games`( `game`, `user`) VALUES (?,?)";
    con.myConn().query(sql, [game_id ,user_id], function (err, result) {
        if (err) throw err;
        console.log(result);

        res.send(result)
    });




})


//insert game account
/*
router.get('/insertGameAccount',upload.single('subjectImage'),function (req,res) {


    var username = req.query.username;
    var email=req.query.email;
    var password=req.query.password ;
    var subjectImage= req.file.subjectImage
    var game_description=req.query.game_description ;
    var game_type=req.query.game_type ;
    const saltRounds = 5;
    // database create an account of type game
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        // set the provider you want from Web3.providers
        web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/'));

    }
    bcrypt.hash(password, saltRounds, function( err, bcryptedPassword) {
        var sql = 'INSERT INTO `user`(`username`, `address`, `password`, `email`, `account_type`, `image`, `game_description`, `game_type`, `enabled`, `private_key`, `date_creation`, `confirmation_token`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';
        var date='2018-05-23'
        account = web3.eth.accounts.create();
        // var date=dateFormat(new Date(), "yyyy-mm-dd h:MM:ss");
        con.myConn().query(sql, [username,account.address,bcryptedPassword,email,"user",req.file.path,game_description,game_type,1,account.privateKey,date,"varrrr"], function (err, result) {
            if (err) throw err;
            console.log(result);

            res.send([{statut:"success"}])
        });
    });



})

*/
router.post('/insertUser',upload.single('subjectImage'),function (req,res) {


    var username = req.body.username;
    var email=req.body.email;
    var password=req.body.password ;
    var subjectImage= req.body.subjectImage || req.file.path ;
    const saltRounds = 5;
    // database create an account of type game
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        // set the provider you want from Web3.providers
        web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/'));

    }
    bcrypt.hash(password, saltRounds, function( err, bcryptedPassword) {
        account = web3.eth.accounts.create();
        var sql = 'INSERT INTO `user`(`username`, `address`, `password`, `email`, `account_type`, `image`, `enabled`, `private_key`, `date_creation`, `confirmation_token`) VALUES (?,?,?,?,?,?,?,?,?,?)';
        var date='2018-05-23'
        // var date=dateFormat(new Date(), "yyyy-mm-dd h:MM:ss");
        con.myConn().query(sql, [username,account.address,bcryptedPassword,email,"user",req.file.path,1,account.privateKey,date,"varrrr"], function (err, result) {
            if (err) throw err;
            console.log(result);

            res.send([{statut:result[0]}])
        });
    });




})


/****************************************************************/




/*********************** web3 methode*******************************/

router.get('/send',function (req,res) {
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {

        web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/6Eln6WdYNncc1lY1zSih'));


    }


    var rawTransaction = {
        "chainId": 0x03,
        value: '0x00',

    };
    contract  =new  web3.eth.Contract(abi,contractAddress);

    var from = req.query.from;
    var to=req.query.to;
    var ammount=req.query.ammount ;
    var pKey=req.query.privateKey;

    rawTransaction.from = from; // as you have set in example
    rawTransaction.to = contractAddress;
    rawTransaction.data = contract.methods.topUp(to, Number(ammount)*10000).encodeABI();

    web3.eth.getGasPrice()
        .then(function (gasPrice){

            web3.eth.getTransactionCount(from).then(function (count){
                var gas = 999999;
                console.log('data')
                console.log(gas);
                console.log(gasPrice);
                rawTransaction.gasLimit= web3.utils.toHex(gas),
                    rawTransaction.gasPrice= web3.utils.toHex(Number(gasPrice));
                rawTransaction.nonce = web3.utils.toHex(count);
                var privKey = new Buffer(pKey, "hex")
                var tx = new Tx(rawTransaction);
                tx.sign(privKey);
                var serializedTx = tx.serialize();
                web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function(err, hash) {
                    if (!err){

                        var sql1 = "INSERT INTO transaction (tx, sender, receiver, type,ammount) VALUES (?,?,?,?,?)";

                        con.myConn().query(sql1, [hash,from,to,"transfer",Number(ammount)*10000], function (err, result) {
                        });
                        console.log("ok " + hash);
                        res.send({hash:hash});
                    }
                    else{
                        console.log("error " + err);

                    }
                });
            });
        });

})





router.get('/getBalance',function (req,res) {

    var user_address = req.query.user_address;
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        // set the provider you want from Web3.providers
        web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/'));
        //  var web3 = new Web3(new Web3.providers.IpcProvider('\\\\.\\pipe\\geth.ipc', net)); // mac os path

    }
    web3.eth.defaultAccount = user_address;

    contract  =new  web3.eth.Contract(abi,contractAddress);
    var balance ;
    contract.methods.balanceOf(user_address).call( function(error, result){

        console.log(result);
        if(!error)
            balance = result;
        else
            console.log(error);
        res.send( balance);

    });


})



router.get('/getGameUnitPrice',function (req,res) {

    var game_address = req.query.game_address;
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        // set the provider you want from Web3.providers
        web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/'));
        //  var web3 = new Web3(new Web3.providers.IpcProvider('\\\\.\\pipe\\geth.ipc', net)); // mac os path

    }
    web3.eth.defaultAccount = game_address;

    contract  =new  web3.eth.Contract(abi,contractAddress);
    var balance ;
    contract.methods.getGameUnitPrice(game_address).call( function(error, result){

        console.log(result);
        if(!error)
            balance = result;
        else
            console.log(error);
       res.send( balance);

    });


})




router.get('/setGameUnitPrice',function (req,res) {

    var game_address = req.query.game_address;
    var gameUnit=req.query.gameUnit;
    var pKey=req.query.privateKey;
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {

        web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/6Eln6WdYNncc1lY1zSih'));


    }


    var rawTransaction = {
        "chainId": 0x03,
        value: '0x00',

    };
    contract  =new  web3.eth.Contract(abi,contractAddress);



    rawTransaction.from = game_address; // as you have set in example
    rawTransaction.to = contractAddress;
    rawTransaction.data = contract.methods.setGameUnitPrice(game_address, gameUnit).encodeABI();

    web3.eth.getGasPrice()
        .then(function (gasPrice){

            web3.eth.getTransactionCount(game_address).then(function (count){
                var gas = 999999;
                console.log('data')
                console.log(gas);
                console.log(gasPrice);
                rawTransaction.gasLimit= web3.utils.toHex(gas),
                    rawTransaction.gasPrice= web3.utils.toHex(Number(gasPrice));
                rawTransaction.nonce = web3.utils.toHex(count);
                var privKey = new Buffer(pKey, "hex")
                var tx = new Tx(rawTransaction);
                tx.sign(privKey);
                var serializedTx = tx.serialize();
                web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function(err, hash) {
                    console.log("ok " + hash);
                });
            });
        });


})



router.get('/getBalanceGame',function (req,res) {

    var user_address = req.query.user_address;
    var game_address = req.query.game_address;
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        // set the provider you want from Web3.providers
        web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/'));
        //  var web3 = new Web3(new Web3.providers.IpcProvider('\\\\.\\pipe\\geth.ipc', net)); // mac os path

    }
    web3.eth.defaultAccount = user_address;

    contract  =new  web3.eth.Contract(abi,contractAddress);
    var balance ;
    contract.methods.gameBalanceOf(user_address,game_address).call( function(error, result){

        console.log(result);
        if(!error)
            balance = result;
        else
            console.log(error);
        res.send( balance);

    });


})





router.post('/topUp',function (req,res) {

    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {

        web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/6Eln6WdYNncc1lY1zSih'));


    }


    var rawTransaction = {
        "chainId": 0x03,
        value: '0x00',

    };
    contract  =new  web3.eth.Contract(abi,contractAddress);

    var sender = req.query.sender;
    var receiver=req.query.receiver;
    var ammount=req.query.ammount ;
    var pKey=req.query.privateKey;

    rawTransaction.from = sender; // as you have set in example
    rawTransaction.to = contractAddress;
    rawTransaction.data = contract.methods.topUp(sender,receiver, Number(ammount)*10000).encodeABI();

    web3.eth.getGasPrice()
        .then(function (gasPrice){

            web3.eth.getTransactionCount(sender).then(function (count){
                var gas = 999999;
                console.log('data')
                console.log(gas);
                console.log(gasPrice);
                rawTransaction.gasLimit= web3.utils.toHex(gas),
                    rawTransaction.gasPrice= web3.utils.toHex(Number(gasPrice));
                rawTransaction.nonce = web3.utils.toHex(count);
                var privKey = new Buffer(pKey, "hex")
                var tx = new Tx(rawTransaction);
                tx.sign(privKey);
                var serializedTx = tx.serialize();
                web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function(err, hash) {
                    if (!err){

                        var sql1 = "INSERT INTO transaction (tx, sender, receiver, type,ammount) VALUES (?,?,?,?,?)";

                        con.myConn().query(sql1, [hash,43,44,"transfer",Number(ammount)*10000], function (err, result) {
                        });
                        console.log("ok " + hash);
                        res.send({hash:hash});
                    }
                    else{
                        console.log("error " + err);

                    }
                });
            });
        });




})





router.post('/cashOut',function (req,res) {


    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {

        web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/6Eln6WdYNncc1lY1zSih'));


    }


    var rawTransaction = {
        "chainId": 0x03,
        value: '0x00',

    };
    contract  =new  web3.eth.Contract(abi,contractAddress);

    var sender = req.query.sender;
    var receiver=req.query.receiver;
    var ammount=req.query.ammount ;
    var pKey=req.query.privateKey;

    rawTransaction.from = sender; // as you have set in example
    rawTransaction.to = contractAddress;
    rawTransaction.data = contract.methods.cashOut(sender,receiver, Number(ammount)*10000).encodeABI();

    web3.eth.getGasPrice()
        .then(function (gasPrice){

            web3.eth.getTransactionCount(sender).then(function (count){
                var gas = 999999;
                console.log('data')
                console.log(gas);
                console.log(gasPrice);
                rawTransaction.gasLimit= web3.utils.toHex(gas),
                    rawTransaction.gasPrice= web3.utils.toHex(Number(gasPrice));
                rawTransaction.nonce = web3.utils.toHex(count);
                var privKey = new Buffer(pKey, "hex")
                var tx = new Tx(rawTransaction);
                tx.sign(privKey);
                var serializedTx = tx.serialize();
                web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function(err, hash) {
                    if (!err){

                        var sql1 = "INSERT INTO transaction (tx, sender, receiver, type,ammount) VALUES (?,?,?,?,?)";

                        con.myConn().query(sql1, [hash,43,44,"transfer",Number(ammount)*10000], function (err, result) {
                        });
                        console.log("ok " + hash);
                        res.send({hash:hash});
                    }
                    else{
                        console.log("error " + err);

                    }
                });
            });
        });



})



/*******************************************************************/

/*************** send mail ****************************/
router.get('/snedMail',function (req,res) {



    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'playcoin.sim3@gmail.com',
            pass: 'playcoinsim3'
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    var mailOptions = {

        from: req.query.from,
        to: 'playcoin.sim3@gmail.com',
        subject: req.query.subject,
        text: req.query.text
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });


    res.send([{statut:"success"}])


})

module.exports=router;